# Sử dụng một base image
FROM python:3.9

# Đặt thư mục làm việc trong container
WORKDIR /app

# Sao chép tệp requirements.txt và cài đặt các gói
RUN pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

# Sao chép toàn bộ dự án vào container
COPY . .

# Expose cổng mà ứng dụng chạy trên
EXPOSE 8000

# Lệnh khởi động ứng dụng
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

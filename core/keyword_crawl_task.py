import json

from django.db import transaction

from apps.crawl.keyword.models import CrawlKeyword
from apps.crawl.keyword.repository import PeriodicTaskRepository, IntervalScheduleRepository, CrawlKeywordRepository


class KeywordCrawlTask:
    def __init__(self, crawl_keyword: CrawlKeyword):
        self.crawl_keyword = crawl_keyword

    def create_keyword_task(self, task_name):
        init_data = {
            "name": task_name,
            "task": 'apps.celery_tasks.scheduled_tasks.tasks.crawl_keyword_job',
            "kwargs": json.dumps(self.generate_input_message()),
            "interval": self.get_or_create_interval(),
            "enabled": 1
        }

        periodic_task = PeriodicTaskRepository.create(init_data)
        if periodic_task:
            CrawlKeywordRepository.update(self.crawl_keyword,
                                          {'periodic_task': periodic_task})

    def update_keyword_task(self, update_info):
        periodic_task_name = update_info.get('periodic_task_name')
        if periodic_task_name:
            del update_info['periodic_task_name']
        with (transaction.atomic()):
            new_crawl_keyword = CrawlKeywordRepository.update(self.crawl_keyword,
                                                              update_info)
            periodic_update = {}
            if periodic_task_name:
                periodic_update['periodic_task_name'] = periodic_task_name
            if update_info.get('interval_time') or update_info.get('time_unit'):
                periodic_update["interval"] = self.get_or_create_interval(new_crawl_keyword)

            if self.crawl_keyword.periodic_task:
                PeriodicTaskRepository.update(self.crawl_keyword.periodic_task,
                                              periodic_update)
            else:
                raise ValueError("This keyword doesn't have any task config")

    def get_or_create_interval(self, crawl_keyword=None):
        if crawl_keyword is None:
            crawl_keyword = self.crawl_keyword
        interval_filter = {
            "every": crawl_keyword.interval_time,
            "period": crawl_keyword.time_unit,
        }
        interval_object = (IntervalScheduleRepository.
                           get_by_filter(interval_filter))

        if interval_object is None:
            interval_object = IntervalScheduleRepository.create(interval_filter)
        return interval_object

    def generate_input_message(self):
        crawl_keyword_list = self.get_crawl_keyword(
            self.crawl_keyword.sub_keyword
        )
        input_message = {
            "social_network": "YOUTUBE",
            "account": {
                "username": None,
                "password": None
            },
            "mode":
                {
                    "id": 3,
                    "script_params": crawl_keyword_list,
                    "type_auto": [],
                    "comments_text": []
                },
            "kafka_topic_handle": self.crawl_keyword.kafka_topic_handle
        }
        return input_message

    def get_crawl_keyword(self, raw_keyword_dict):
        keys = []
        for item in raw_keyword_dict:
            key_value = item.get("key")
            if key_value:
                keys.append(key_value)
        return keys

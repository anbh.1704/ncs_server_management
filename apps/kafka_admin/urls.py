from rest_framework import routers

from apps.kafka_admin.v1.views import KafkaAdminApiView

router = routers.DefaultRouter()
router.register('kafka-admin', KafkaAdminApiView,
                basename='kafka-admin')
urlpatterns = router.urls

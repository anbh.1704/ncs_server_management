import json

import requests
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.crawl.crawl_bot.repository import CrawlBotRepository
from apps.crawl.crawl_server.repository import CrawlServerRepository
from apps.kafka_admin.kafka_admin import KafkaAdminManagement, producer
from apps.kafka_admin.v1.serializer import KafkaAdminSerializer
from apps.users.user_repository import UserRepository
from core.base_model_view_set import BaseGenericViewSet, BaseListModelMixin


class KafkaAdminApiView(BaseGenericViewSet,
                        BaseListModelMixin):
    authentication_classes = []
    serializer_class = KafkaAdminSerializer
    repository_class = UserRepository

    def list(self, request, *args, **kwargs):
        return Response(KafkaAdminManagement().list_all_topic())

    @action(methods=['DELETE'], detail=False)
    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'topic_name': openapi.Schema(type=openapi.TYPE_STRING),
        }
    ))
    def delete_topic(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        kafka_admin = KafkaAdminManagement()
        topic_name = serializer.validated_data.get('topic_name')
        ids_by_server = CrawlBotRepository.get_by_topics([topic_name])
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
        }

        for server_id in ids_by_server:
            server_info = CrawlServerRepository.get_by_id(server_id)
            payload = json.dumps(ids_by_server[server_id])
            remote_api = (f'http://{server_info.ip_address}:{server_info.port}/api/v1/crawler/bot/')
            response = requests.delete(remote_api, headers=headers, data=payload)
            if response.status_code != 200:
                raise ValidationError(json.loads(response.text)['message'])

        kafka_admin.close_specific_topic(topic_name)
        kafka_admin.close()
        return Response(serializer.data)

    @action(methods=['DELETE'], detail=False)
    def delete_all_topic(self, request, *args, **kwargs):
        kafka_admin = KafkaAdminManagement()
        topics = kafka_admin.admin_client.list_topics()
        ids_by_server = CrawlBotRepository.get_by_topics(topics)
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
        }
        for server_id in ids_by_server:
            server_info = CrawlServerRepository.get_by_id(server_id)
            payload = json.dumps(ids_by_server[server_id])
            remote_api = (f'http://{server_info.ip_address}:{server_info.port}/api/v1/crawler/bot/')
            response = requests.delete(remote_api, headers=headers, data=payload)

            if response.status_code != 200:
                raise ValidationError(json.loads(response.text)['message'])

        kafka_admin.close_all_topic()
        kafka_admin.close()
        return Response()

    @action(methods=['POST'], detail=False)
    def create_topic(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        topic_name = serializer.validated_data.get('topic_name')
        partitions = serializer.validated_data.get('partition')
        kafka_admin = KafkaAdminManagement()
        kafka_admin.create_topic(topic_name, partitions)
        kafka_admin.close()
        return Response()

    @action(methods=['POST'], detail=False)
    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['topic_name', 'messages'],
        properties={
            'topic_name': openapi.Schema(type=openapi.TYPE_STRING),
            'messages': openapi.Schema(type=openapi.TYPE_ARRAY,
                                       items=openapi.Schema(type=openapi.TYPE_OBJECT)),
        }
    ))
    def message_topic(self, request, *args, **kwargs):
        topic_name = request.data.get('topic_name')
        messages = request.data.get('messages')
        for message in messages:
            producer.send(topic_name, message)
        return Response()

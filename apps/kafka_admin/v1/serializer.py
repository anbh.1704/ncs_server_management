from rest_framework import serializers


class KafkaAdminSerializer(serializers.Serializer):
    topic_name = serializers.CharField(max_length=255)
    partition = serializers.IntegerField(default=10)

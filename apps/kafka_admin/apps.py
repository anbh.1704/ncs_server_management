from django.apps import AppConfig


class KafkaAdminConfig(AppConfig):
    name = 'apps.kafka_admin'

import json

from kafka import KafkaProducer
from kafka.admin import KafkaAdminClient, NewTopic

from core.my_settings import KAFKA_SERVER_URL


class KafkaAdminManagement:
    def __init__(self):
        self.admin_client = KafkaAdminClient(
            bootstrap_servers=KAFKA_SERVER_URL
        )

    def create_topic(self, topic_name, num_partitions=10,
                     replication_factor=1):
        new_topic = NewTopic(name=topic_name, num_partitions=num_partitions,
                             replication_factor=replication_factor)
        self.admin_client.create_topics(new_topics=[new_topic])

    def list_all_topic(self):
        topic_metadata = self.admin_client.list_topics()
        topic_names = topic_metadata
        topic_partitions = self.admin_client.describe_topics(topic_names)
        result = []
        for topic_info in topic_partitions:
            topic_name = topic_info.get('topic')
            num_partitions = len(topic_info.get('partitions'))
            result.append({
                'topic_name': topic_name,
                'partitions': num_partitions
            })
        return result

    def close_specific_topic(self, topic_name):
        topic_metadata = self.admin_client.list_topics()
        if topic_name in topic_metadata:
            self.admin_client.delete_topics([topic_name])
        else:
            raise ValueError(f"The topic = {topic_name} doesn't exist")

    def close_all_topic(self):
        topic_metadata = self.admin_client.list_topics()

        for topic_name in topic_metadata:
            if topic_name != '__consumer_offsets':
                self.admin_client.delete_topics([topic_name])

    def close(self):
        self.admin_client.close()


producer = KafkaProducer(bootstrap_servers=[KAFKA_SERVER_URL],
                         value_serializer=lambda v: json.dumps(v).encode('utf-8'))

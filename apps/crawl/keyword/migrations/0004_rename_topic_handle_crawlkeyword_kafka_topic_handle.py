# Generated by Django 3.2.21 on 2023-10-06 16:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('keyword', '0003_crawlkeyword_topic_handle'),
    ]

    operations = [
        migrations.RenameField(
            model_name='crawlkeyword',
            old_name='topic_handle',
            new_name='kafka_topic_handle',
        ),
    ]

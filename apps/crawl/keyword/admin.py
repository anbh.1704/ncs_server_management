from django.contrib import admin

# Register your models here.
from .models import CrawlKeyword

admin.site.register(CrawlKeyword)

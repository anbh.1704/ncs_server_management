from rest_framework import serializers

from apps.crawl.keyword.models import CrawlKeyword


class CrawlKeywordSerializer(serializers.ModelSerializer):
    class Meta:
        model = CrawlKeyword
        fields = ('id', "name", 'keyword_includes',
                  'keylink_includes', 'sub_keyword', "id_topic",
                  "periodic_task",
                  "interval_time", "time_unit", "kafka_topic_handle")


class CrawlKeywordCreateSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=256, required=False)
    keyword_includes = serializers.CharField(max_length=256, required=False)
    keylink_includes = serializers.CharField(max_length=1000, required=False)
    sub_keyword = serializers.JSONField(required=False)
    id_topic = serializers.IntegerField(required=False)
    periodic_task_name = serializers.CharField(max_length=256, required=False)
    interval_time = serializers.FloatField(required=False)
    time_unit = serializers.CharField(max_length=100, required=False)
    kafka_topic_handle = serializers.CharField(max_length=1000, required=False)

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.response import Response

from apps.crawl.keyword.models import CrawlKeyword
from apps.crawl.keyword.repository import CrawlKeywordRepository, PeriodicTaskRepository
from apps.crawl.keyword.v1.filters import CrawlerKeywordFilter
from apps.crawl.keyword.v1.serializer import CrawlKeywordCreateSerializer, CrawlKeywordSerializer
from core.base_model_view_set import BaseCreateModelMixin, BaseGenericViewSet, BaseUpdateModelMixin, \
    BaseDestroyModelMixin, BaseListModelMixin
from core.keyword_crawl_task import KeywordCrawlTask


class CrawlKeywordApiView(BaseCreateModelMixin,
                          BaseUpdateModelMixin,
                          BaseDestroyModelMixin,
                          BaseListModelMixin,
                          BaseGenericViewSet):
    permission_classes = []
    authentication_classes = []
    serializer_action_classes = {
        "list": CrawlKeywordSerializer
    }
    serializer_class = CrawlKeywordCreateSerializer
    repository_class = CrawlKeywordRepository
    filter_backends = [DjangoFilterBackend]
    filterset_class = CrawlerKeywordFilter

    def create(self, request, *args, **kwargs):
        request_data = request.data
        task_name = request_data.get('periodic_task_name')
        del request_data['periodic_task_name']
        crawl_keyword: CrawlKeyword = CrawlKeywordRepository.create(request_data)
        crawl_keyword_task = KeywordCrawlTask(crawl_keyword)

        try:
            crawl_keyword_task.create_keyword_task(task_name)
        except BaseException as e:
            CrawlKeywordRepository.delete(crawl_keyword_task.crawl_keyword)
            raise ValueError(str(e))
        return Response(status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        request_data = request.data
        crawl_keyword = CrawlKeywordRepository.get_by_id(kwargs.get('pk'))
        if crawl_keyword is None:
            raise ValueError("This keyword config doesn't exist")
        crawl_keyword_task = KeywordCrawlTask(crawl_keyword)
        crawl_keyword_task.update_keyword_task(request_data)
        return Response("Updated")

    def destroy(self, request, *args, **kwargs):
        instance: CrawlKeyword = CrawlKeywordRepository.get_by_id(kwargs.get('pk'))
        if instance is None:
            raise ValueError("Keyword with this id doesn't exist")
        CrawlKeywordRepository.delete(instance)
        if instance.periodic_task:
            PeriodicTaskRepository.delete(instance.periodic_task)
        return Response()

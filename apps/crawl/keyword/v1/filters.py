from django_filters import FilterSet

from core.filters import FilterSetMixin


class CrawlerKeywordFilter(FilterSetMixin, FilterSet):
    pass

from django_celery_beat.models import PeriodicTask, IntervalSchedule

from apps.crawl.keyword.models import CrawlKeyword
from core.base_repository import (RepositoryBase)


class CrawlKeywordRepository(RepositoryBase):
    class Meta:
        model = CrawlKeyword


class PeriodicTaskRepository(RepositoryBase):
    class Meta:
        model = PeriodicTask


class IntervalScheduleRepository(RepositoryBase):
    class Meta:
        model = IntervalSchedule

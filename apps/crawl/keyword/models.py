from django.db import models
from django_celery_beat.models import PeriodicTask


class CrawlKeyword(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256, null=True, blank=True)
    keyword_includes = models.CharField(max_length=256, null=True, blank=True)
    keylink_includes = models.CharField(max_length=1000, null=True, blank=True)
    sub_keyword = models.JSONField(null=True, blank=True)
    id_topic = models.IntegerField(null=True, blank=True)
    periodic_task = models.ForeignKey(PeriodicTask, on_delete=models.CASCADE,
                                      null=True, blank=True)
    interval_time = models.FloatField(null=True, default=4.0, blank=True)
    time_unit = models.CharField(null=True, blank=True, default="Hour", max_length=100)
    kafka_topic_handle = models.CharField(null=True, max_length=1000, blank=True)

    class Meta:
        db_table = 'crawl_keyword'

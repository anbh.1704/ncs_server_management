from rest_framework import routers

from apps.crawl.crawl_bot.v1.views import CrawlerBotApiView
from apps.crawl.crawl_proxy.v1.views import CrawlerProxyApiView
from apps.crawl.crawl_server.v1.views import CrawlerServerApiView
from apps.crawl.keyword.v1.views import CrawlKeywordApiView

router = routers.DefaultRouter()
router.register('crawl/server', CrawlerServerApiView, basename='crawl')
router.register('crawl/bot', CrawlerBotApiView, basename='crawl')
router.register('crawl/proxy', CrawlerProxyApiView, basename='crawl')
router.register('crawl/keyword', CrawlKeywordApiView, basename='crawl')
urlpatterns = router.urls

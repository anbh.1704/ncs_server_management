from django.db import models
from django.utils import timezone


class CrawlServer(models.Model):
    name = models.CharField(max_length=255, blank=True)
    ip_address = models.CharField(max_length=20, blank=True)
    port = models.PositiveIntegerField(blank=True)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'crawl_server'
        unique_together = ('ip_address', 'port')

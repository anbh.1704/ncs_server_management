from django.db.models import Count

from apps.crawl.crawl_server.models import CrawlServer
from core.base_repository import (RepositoryBase)


class CrawlServerRepository(RepositoryBase):
    class Meta:
        model = CrawlServer

    @classmethod
    def all(cls):
        return cls.Meta.model.objects.all().annotate(
            bot_count=Count('crawlbot'))

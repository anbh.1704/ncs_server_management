# Generated by Django 3.2.21 on 2023-10-04 10:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crawl_server', '0004_auto_20231003_0758'),
    ]

    operations = [
        migrations.AlterField(
            model_name='crawlserver',
            name='ip_address',
            field=models.CharField(blank=True, max_length=20),
        ),
        migrations.AlterField(
            model_name='crawlserver',
            name='name',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='crawlserver',
            name='port',
            field=models.PositiveIntegerField(blank=True),
        ),
    ]

from rest_framework import serializers

from apps.crawl.crawl_server.models import CrawlServer


class CrawlServerListSerializer(serializers.ModelSerializer):
    bot_count = serializers.IntegerField()
    class Meta:
        model = CrawlServer
        fields = ('id', 'name', 'ip_address',
                  'port', 'status', "bot_count")


class CrawlServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = CrawlServer
        fields = ('id', 'name', 'ip_address', 'port')

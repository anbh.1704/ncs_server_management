from django_filters.rest_framework import DjangoFilterBackend

from apps.crawl.crawl_server.repository import CrawlServerRepository
from apps.crawl.crawl_server.v1.filters import CrawlServerFilter
from apps.crawl.crawl_server.v1.serializer import CrawlServerSerializer, CrawlServerListSerializer
from core.base_model_view_set import BaseModelViewSet


class CrawlerServerApiView(BaseModelViewSet):
    authentication_classes = []
    serializer_action_classes = {
        'list': CrawlServerListSerializer
    }
    permission_classes = []
    serializer_class = CrawlServerSerializer
    repository_class = CrawlServerRepository
    filter_backends = [DjangoFilterBackend]
    filterset_class = CrawlServerFilter

from django_filters import FilterSet, NumberFilter, CharFilter

from core.filters import FilterSetMixin


class CrawlServerFilter(FilterSetMixin, FilterSet):
    name = CharFilter(lookup_expr='icontains')




from django.contrib import admin

# Register your models here.
from .models import CrawlServer

admin.site.register(CrawlServer)

import json

import requests
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response

from apps.crawl.crawl_bot.repository import CrawlBotRepository
from apps.crawl.crawl_bot.v1.filters import CrawlerBotFilter
from apps.crawl.crawl_bot.v1.serializer import CrawlerBotCreateSerializer, CrawlerBotSerializer, \
    CrawlerBotDeleteSerializer
from apps.crawl.crawl_server.models import CrawlServer
from apps.crawl.crawl_server.repository import CrawlServerRepository
from core.base_model_view_set import BaseGenericViewSet, BaseListModelMixin


class CrawlerBotApiView(BaseGenericViewSet, BaseListModelMixin):
    serializer_action_classes = {
        'create_bot': CrawlerBotCreateSerializer,
        'delete_bot': CrawlerBotDeleteSerializer
    }
    permission_classes = []
    authentication_classes = []
    serializer_class = CrawlerBotSerializer
    repository_class = CrawlBotRepository
    filter_backends = [DjangoFilterBackend]
    filterset_class = CrawlerBotFilter

    @action(methods=['POST'], detail=False)
    def create_bot(self, request, *args, **kwargs):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
        }
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        server_info: CrawlServer = CrawlServerRepository.get_by_id(
            serializer.validated_data.get('server_id')
        )
        remote_api = f'http://{server_info.ip_address}:{server_info.port}/api/v1/crawler/bot'
        payload = {
            "server_id": serializer.validated_data.get('server_id'),
            "topic": serializer.validated_data.get('topic'),
            "group_id": serializer.validated_data.get('group_id'),
            "amount": serializer.validated_data.get('amount')
        }
        response = requests.post(remote_api,
                                 headers=headers,
                                 json=payload,
                                 timeout=20)
        if response.status_code == 201:
            return Response(response.json())
        else:
            raise ValidationError(json.loads(response.text)['message'])

    @action(methods=['DELETE'], detail=False)
    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'ids': openapi.Schema(type=openapi.TYPE_ARRAY,
                                  items=openapi.Schema(type=openapi.TYPE_INTEGER)),
            'server_id': openapi.Schema(type=openapi.TYPE_INTEGER),
        }
    ))
    def delete_bot(self, request, *args, **kwargs):
        headers = {
            'accept': 'application/json',
            'Content-Type': 'application/json',
        }
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        server_info: CrawlServer = CrawlServerRepository.get_by_id(
            serializer.validated_data.get('server_id')
        )
        ids = serializer.validated_data.get('ids')
        payload = json.dumps(ids)
        remote_api = (f'http://{server_info.ip_address}:{server_info.port}/api/v1/crawler/bot/')
        response = requests.delete(remote_api, headers=headers, data=payload)
        if response.status_code == 200:
            return Response(response.json())
        else:
            raise ValidationError(json.loads(response.text)['message'])

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        filter_kwargs = self.get_filter_kwargs()
        queryset = self.repository_class.filter_queryset(queryset, filter_kwargs=filter_kwargs)

        if self.ordering_fields:
            ordering = OrderingFilter().get_ordering(self.request, queryset, self)
            queryset = self.repository_class.ordering_queryset(queryset, ordering)
        if self.pagination_class is not None:
            page = self.paginate_queryset(queryset)
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

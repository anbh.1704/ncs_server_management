from django_filters import FilterSet, NumberFilter, CharFilter

from core.filters import FilterSetMixin


class CrawlerBotFilter(FilterSetMixin, FilterSet):
    id = NumberFilter()
    group_id = CharFilter(lookup_expr='icontains')
    topic = CharFilter(lookup_expr='icontains')
    social = NumberFilter()
    server = NumberFilter()




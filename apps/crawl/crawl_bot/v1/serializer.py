from rest_framework import serializers

from apps.crawl.crawl_bot.models import CrawlBot
from apps.crawl.crawl_bot.repository import (
    CrawlServerRepository)


class CrawlerBotCreateSerializer(serializers.Serializer):
    server_id = serializers.IntegerField()
    topic = serializers.CharField(max_length=255)
    group_id = serializers.CharField(max_length=255)
    amount = serializers.IntegerField()


class CrawlerBotSerializer(serializers.ModelSerializer):
    class Meta:
        model = CrawlBot
        fields = ('id', "server", 'topic', 'group_id', 'status',
                  "current_script", "current_params",
                  'social_type',)


class CrawlerBotDeleteSerializer(serializers.Serializer):
    ids = serializers.ListField(child=serializers.IntegerField())
    server_id = serializers.IntegerField()

    def validate_server_id(self, value):
        if not CrawlServerRepository.check_exist_by_filter({'id': value}):
            raise ValueError(f"This server with id = {value} doesn't exist")
        return value

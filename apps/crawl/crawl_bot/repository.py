from apps.crawl.crawl_bot.models import CrawlBot
from apps.crawl.crawl_server.models import CrawlServer
from core.base_repository import (RepositoryBase)


class CrawlServerRepository(RepositoryBase):
    class Meta:
        model = CrawlServer


class CrawlBotRepository(RepositoryBase):
    class Meta:
        model = CrawlBot

    @classmethod
    def get_by_topics(cls, topics):
        bots = cls.Meta.model.objects.filter(
            topic__in=topics
        )
        bots_by_server_id = {}

        for bot in bots:
            if bots_by_server_id.get(bot.server.id):
                bots_by_server_id[bot.server.id].append(bot.id)
            else:
                bots_by_server_id[bot.server.id] = [bot.id]

        return bots_by_server_id

from enum import Enum

from django.db import models
from django.utils import timezone

from apps.crawl.crawl_server.models import CrawlServer


class Social(Enum):
    YOUTUBE = 1
    TIKTOK = 2
    FACEBOOK = 3


class CrawlBot(models.Model):
    class Status(Enum):
        RUNNING = 1
        WAITING = 0

    topic = models.CharField(max_length=255)
    group_id = models.CharField(max_length=20)
    status = models.PositiveIntegerField(default=Status.WAITING.value)
    social_type = models.PositiveIntegerField(null=True)
    server = models.ForeignKey(CrawlServer, on_delete=models.SET_NULL, null=True)
    current_script = models.SmallIntegerField(null=True)
    current_params = models.CharField(max_length=1000, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'crawl_bot'

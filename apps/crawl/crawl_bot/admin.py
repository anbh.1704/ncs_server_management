from django.contrib import admin

# Register your models here.
from .models import CrawlBot

admin.site.register(CrawlBot)

from django.contrib import admin

# Register your models here.
from .models import CrawlProxy

admin.site.register(CrawlProxy)

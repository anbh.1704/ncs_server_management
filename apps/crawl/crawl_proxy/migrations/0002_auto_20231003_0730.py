# Generated by Django 3.2.21 on 2023-10-03 07:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crawl_proxy', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='crawlproxy',
            old_name='ip_address',
            new_name='ipaddress',
        ),
        migrations.AlterUniqueTogether(
            name='crawlproxy',
            unique_together={('ipaddress', 'port')},
        ),
    ]

from enum import Enum

from django.db import models
from django.utils import timezone

from apps.crawl.crawl_bot.models import CrawlBot
from apps.crawl.crawl_server.models import CrawlServer


class Status(Enum):
    ON = 1
    OFF = 0


class CrawlProxy(models.Model):
    ip_address = models.CharField(max_length=255, blank=True)
    port = models.PositiveIntegerField(blank=True)
    status = models.IntegerField(default=Status.ON.value)
    username = models.CharField(max_length=255, blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    crawl_bot = models.ForeignKey(CrawlBot, on_delete=models.CASCADE,
                                  null=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'crawl_proxy'

import requests
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.response import Response

from apps.crawl.crawl_proxy.repository import CrawlProxyRepository
from apps.crawl.crawl_proxy.v1.filters import CrawlProxyFilter
from apps.crawl.crawl_proxy.v1.serializer import CrawlProxySerializer, CrawlProxyCreateSerializer
from config.settings import PROXY_SERVER_URL
from core.base_model_view_set import BaseModelViewSet


class CrawlerProxyApiView(BaseModelViewSet):
    permission_classes = []
    authentication_classes = []
    serializer_class = CrawlProxySerializer
    serializer_action_classes = {
        'create': CrawlProxyCreateSerializer
    }
    repository_class = CrawlProxyRepository
    filter_backends = [DjangoFilterBackend]
    filterset_class = CrawlProxyFilter

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        ip = serializer.validated_data.get('ip_address')
        port = serializer.validated_data.get('port')
        proxy_info = f'{ip}:{port}'
        request_url = f'{PROXY_SERVER_URL}/status?proxy={proxy_info}'
        try:
            response = requests.get(request_url, timeout=3)
            response.raise_for_status()
            response_json = response.json()
            proxy_status = response_json.get('status')
        except requests.exceptions.Timeout:
            proxy_status = 0
        except BaseException as e:
            raise ValueError(str(e))
        serializer.validated_data['status'] = 1 if proxy_status else 0
        instance = self.repository_class.create(serializer.validated_data)
        headers = self.get_success_headers(serializer.data)
        return Response(self.get_serializer(instance).data,
                        status=status.HTTP_201_CREATED, headers=headers)

from rest_framework import serializers

from apps.crawl.crawl_proxy.models import CrawlProxy


class CrawlProxySerializer(serializers.ModelSerializer):
    class Meta:
        model = CrawlProxy
        fields = ('id', 'ip_address', 'port', 'username', 'status',
                  "crawl_bot",
                  'password')


class CrawlProxyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CrawlProxy
        fields = ('id', 'ip_address', 'port', 'username',
                  'password')

from django_filters import FilterSet, NumberFilter, CharFilter

from core.filters import FilterSetMixin


class CrawlProxyFilter(FilterSetMixin, FilterSet):
    name = CharFilter(lookup_expr='icontains')
    type = CharFilter(lookup_expr='icontains')



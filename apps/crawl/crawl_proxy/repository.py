from apps.crawl.crawl_proxy.models import CrawlProxy
from core.base_repository import (RepositoryBase)


class CrawlProxyRepository(RepositoryBase):
    class Meta:
        model = CrawlProxy

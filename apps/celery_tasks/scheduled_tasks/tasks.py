import json

from celery import shared_task
from kafka import KafkaProducer

from core.my_settings import KAFKA_SERVER_URL


@shared_task
def crawl_keyword_job(**kwargs):
    producer = KafkaProducer(bootstrap_servers=[KAFKA_SERVER_URL],
                             value_serializer=lambda v: json.dumps(v).encode('utf-8'))

    kafka_topic_handle = kwargs.get('kafka_topic_handle')
    producer.send(kafka_topic_handle, kwargs)

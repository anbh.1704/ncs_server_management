from rest_framework import routers

from apps.users.v1.views import UserApiView

router = routers.DefaultRouter()
router.register('users', UserApiView, basename='user')
urlpatterns = router.urls

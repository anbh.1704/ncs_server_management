from rest_framework.decorators import api_view, authentication_classes
from rest_framework.response import Response

from apps.kafka_admin.kafka_admin import producer


@api_view(['GET'])
@authentication_classes([])
def ping(request):

    return Response(data='pong')

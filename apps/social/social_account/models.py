from django.db import models
from django.utils import timezone

from apps.crawl.crawl_bot.models import CrawlBot
from apps.social.profiles.models import Profile


class SocialAccount(models.Model):
    username = models.CharField(max_length=63, unique=True, null=True, blank=True)
    password = models.CharField(max_length=20, blank=True)
    email = models.EmailField(max_length=63, unique=True, null=True, blank=True)
    pass_email = models.CharField(max_length=255, null=True, blank=True)
    type = models.CharField(max_length=255, null=True, blank=True)
    status = models.IntegerField(default=1, blank=True)
    twofa = models.CharField(max_length=500, null=True, blank=True)
    profile = models.ForeignKey(Profile, on_delete=models.SET_NULL,
                                 null=True, blank=True)
    is_recovery = models.BooleanField(default=False, blank=True)
    recovery_account_id = models.IntegerField(null=True, blank=True)
    crawl_bot = models.ForeignKey(CrawlBot, on_delete=models.CASCADE,
                                  null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'social_account'

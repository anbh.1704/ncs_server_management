from apps.social.social_account.models import SocialAccount
from core.base_repository import (RepositoryBase)


class SocialAccountRepository(RepositoryBase):
    class Meta:
        model = SocialAccount

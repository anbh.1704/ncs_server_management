from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'apps.social.social_account'

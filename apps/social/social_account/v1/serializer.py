from rest_framework import serializers

from apps.social.social_account.models import SocialAccount


class SocialAccountListSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialAccount
        fields = ('id', 'email', 'password', 'username',
                  "type", 'status', "twofa", "profile", "pass_email", "note")


class SocialAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialAccount
        fields = ('id', 'email', 'password', 'username',
                  "type", "twofa", "profile", "pass_email", "note")

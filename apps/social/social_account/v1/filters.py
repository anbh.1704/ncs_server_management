from django_filters import FilterSet, NumberFilter, CharFilter

from core.filters import FilterSetMixin


class SocialAccountFilter(FilterSetMixin, FilterSet):
    username = CharFilter(lookup_expr='icontains')
    type = NumberFilter()
    profile = NumberFilter()




from django_filters.rest_framework import DjangoFilterBackend

from apps.social.social_account.repository import SocialAccountRepository
from apps.social.social_account.v1.filters import SocialAccountFilter
from apps.social.social_account.v1.serializer import SocialAccountSerializer, SocialAccountListSerializer
from core.base_model_view_set import BaseModelViewSet


class SocialAccountApiView(BaseModelViewSet):
    authentication_classes = []
    serializer_class = SocialAccountSerializer
    serializer_action_classes = {
        'list': SocialAccountListSerializer,
    }
    repository_class = SocialAccountRepository
    filter_backends = [DjangoFilterBackend]
    filterset_class = SocialAccountFilter

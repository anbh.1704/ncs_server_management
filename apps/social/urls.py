from rest_framework import routers

from apps.social.profiles.v1.views import ProfileApiView
from apps.social.social_account.v1.views import SocialAccountApiView

router = routers.DefaultRouter()
router.register('social-account', SocialAccountApiView,
                basename='social-account')
router.register('profile', ProfileApiView, basename='profile')
urlpatterns = router.urls

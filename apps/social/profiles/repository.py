from apps.social.profiles.models import Profile
from core.base_repository import (RepositoryBase)


class ProfilesRepository(RepositoryBase):
    class Meta:
        model = Profile

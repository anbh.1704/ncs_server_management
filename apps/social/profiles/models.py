from django.db import models
from django.utils import timezone

from apps.crawl.crawl_bot.models import CrawlBot


class Profile(models.Model):
    user_agent = models.CharField(max_length=500)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'profile'

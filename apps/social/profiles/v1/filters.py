from django_filters import FilterSet, NumberFilter, CharFilter

from core.filters import FilterSetMixin


class ProfileFilter(FilterSetMixin, FilterSet):
    user_agent = CharFilter(lookup_expr='icontains')




from django_filters.rest_framework import DjangoFilterBackend

from apps.social.profiles.repository import ProfilesRepository
from apps.social.profiles.v1.filters import ProfileFilter
from apps.social.profiles.v1.serializer import (ProfileSerializer,
                                                ProfileCreateSerializer)
from core.base_model_view_set import BaseModelViewSet


class ProfileApiView(BaseModelViewSet):
    authentication_classes = []
    serializer_class = ProfileCreateSerializer
    serializer_action_classes = {
        'list': ProfileSerializer,
    }
    repository_class = ProfilesRepository
    filter_backends = [DjangoFilterBackend]
    filterset_class = ProfileFilter
